import React from 'react'

import Characters from '../src/components/organisms/Characters/Character'

function App() {
  return (
    <div className='App'>
      <Characters />
    </div>
  )
}

export default App
