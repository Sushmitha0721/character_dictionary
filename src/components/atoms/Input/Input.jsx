import React from 'react'
import PropTypes from 'prop-types'
import './Input.scss'

const Input = (props) => {
  return (
    <input
      type='text'
      id={props.id}
      name={props.name}
      className='input'
      onChange={props.handleChange}
      placeholder={props.placeholder}
    />
  )
}

Input.propTypes = {
  id: PropTypes.string,
  handleChange: PropTypes.func,
  name: PropTypes.string,
  placeholder: PropTypes.string
}
export default Input
