import React from 'react'
import PropTypes from 'prop-types'
import './Checkbox.scss'

const Checkbox = (props) => {
  return (
    <div className='checkbox'>
      <input
        type='checkbox'
        onChange={props.handleCheckbox}
        id={props.id}
        value={props.label}
        checked={props.isChecked}
      />
      <label htmlFor={props.id} className='checkbox__label'>
        {props.label}
      </label>
    </div>
  )
}
Checkbox.propTypes = {
  id: PropTypes.string,
  handleCheckbox: PropTypes.func,
  label: PropTypes.string,
  isChecked: PropTypes.string
}

export default Checkbox
