/* eslint-disable indent */
import React from 'react'
import PropTypes from 'prop-types'
import './DropdownSelect.scss'

const Select = (props) => {
  return (
    <select className='select' onChange={props.handleChange}>
      {props.options
        ? props.options.map((option, key) => (
            <option value={option.value} key={key}>
              {option.text}
            </option>
          ))
        : null}
    </select>
  )
}

export default Select

Select.propTypes = {
  handleChange: PropTypes.func,
  options: PropTypes.string
}
