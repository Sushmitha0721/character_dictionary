import React from 'react'
import PropTypes from 'prop-types'
import './Button.scss'

const Button = (props) => {
  return (
    // eslint-disable-next-line react/prop-types
    <button className={`btn btn--${props.color}`} onClick={props.handleClick}>
      {props.text}
    </button>
  )
}
Button.propTypes = {
  color: PropTypes.string,
  handleClick: PropTypes.func,
  text: PropTypes.string
}

export default Button
