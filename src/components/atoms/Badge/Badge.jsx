import React from 'react'
import PropTypes from 'prop-types'
import './Badge.scss'

const Badge = (props) => {
  return (
    <div className='badge'>
      <span className='badge__text' title={props.text}>
        {props.text}
      </span>
      <span className='badge__icon' onClick={props.handleClick}>
        <i className={`fa ${props.icon} fa-lg`}></i>
      </span>
    </div>
  )
}
Badge.propTypes = {
  text: PropTypes.string,
  handleClick: PropTypes.func,
  icon: PropTypes.string
}

export default Badge
